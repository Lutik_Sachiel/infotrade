<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/invoices', function () {
    return \App\Invoice::orderBy('id', 'desc')->get();
});

Route::post('/invoices', function (Request $request) {
    $input = $request->all();
    \App\Invoice::insert($input);
    return response()->json($input);
});

Route::put('/invoices/{id}', function (Request $request, $id) {
    $invoice = \App\Invoice::find($id);
    $invoice->number = $request->input('number');
    $invoice->createDate = $request->input('createDate');
    $invoice->supplyDate = $request->input('supplyDate');
    $invoice->comment = $request->input('comment');
    $invoice->save();
    return response()->json($invoice);
});

Route::delete('/invoices/{id}', function ($id) {
    \App\Invoice::find($id)->delete();
    return response()->json('success');
});

